<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>

		<style>.offcanvas { opacity: 0; visibility: hidden; }</style>

	</head>

	<body <?php body_class(); ?>>

	<div id="legacy">
		<div class="container">
			<p>You are using an outdated browser. Please <a href="https://www.google.com/chrome/browser/" target="_blank">upgrade</a> to improve your experience. <small onclick="document.getElementById('legacy').style.display='none'">I get it, <a>take me to the site</a>.</small></p>
		</div>
	</div>

	<header class="header" role="banner">
		<div class="container">
			<?php get_template_part( 'parts/nav', 'topbar' ); ?>
		</div>
	</header>
