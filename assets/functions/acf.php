<?php

if( function_exists('acf_add_options_page') ) {
 
	$option_page = acf_add_options_page(array(
		'position' => 90,
		'page_title' 	=> 'Theme Settings',
		'menu_title' 	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
 
}

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyBkqX8Wol5r77RKjGw39US6Frl3HUfKn18');
}

add_action('acf/init', 'my_acf_init');