<setion class="module__double-text">

	<?php if($container): ?>
		<div class="container<?php if($container_width == "Narrow") : echo "--narrow"; elseif($container_width == "Wide") : echo "--wide"; endif ?>">
	<?php endif; ?>

		<div class="row row--middle row-collapse">

			<?php if( $content_l ): ?>
				<div class="module__content">
					<?php echo $content_l; ?>
				</div>
			<?php endif; ?>

			<?php if( $content_r ): ?>
				<div class="module__content">
					<?php echo $content_r; ?>
				</div>
			<?php endif; ?>

		</div>

		<div class="row row--middle row-collapse">

			<?php if( $button['type'] == 'Page' ): $button_url = $button['page_link'];
				elseif( $button['type'] =='Internal' ): $button_url = $button['internal_link'];
				else: $button_url = $button['external_link'];
			endif; ?>

			<?php if( $button['text']): ?>
				<div class="module__button">
					<a class="button" href="<?php echo $button_url; ?>" <?php if($button['type'] == "External") : ?>target="_blank" rel="noopener noreferrer"<?php endif; ?>><?php echo $button['text']; ?></a>
				</div>
			<?php endif; ?>

		</div>

	<?php if($container): ?>
		</div>
	<?php endif; ?>

</section>
