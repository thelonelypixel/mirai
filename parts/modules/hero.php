<section class="module module__hero">
	<div class="container">
		<div class="row row--middle">
			<div class="module__hero__content">
				<?php if($hero_content) :
					echo $hero_content;
				endif; ?>

				<?php if( $button['type'] == 'Page' ): $button_url = $button['page_link'];
					elseif( $button['type'] =='Internal' ): $button_url = $button['internal_link'];
					else: $button_url = $button['external_link'];
				endif;

				if( $button['text'] ): ?>
					<!-- Button -->
					<a class="button" href="<?php echo $button_url; ?>" <?php if($button['type'] == "External") : ?>target="_blank" rel="noopener noreferrer"<?php endif; ?>><?php echo $button['text']; ?></a>
				<?php endif; ?>
			</div>
			<div class="module__hero__image">
				<?php
				$image = get_sub_field('hero_image');
				$size = 'full'; // (thumbnail, medium, large, full or custom size)

				if ( $image ) : ?>
				  <?php echo wp_get_attachment_image( $image, $size ); ?>
				<?php endif;?>
			</div>
		</div>
	</div>
	<div class="overlay"></div>
</section>

<div id="content"></div>
