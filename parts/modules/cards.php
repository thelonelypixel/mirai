<?php

// Global Vars

// Options

$columns = get_sub_field('columns');

// Intro

$intro = get_sub_field('add_intro');
$intro_heading = get_sub_field('heading');
$intro_content = get_sub_field('intro');

?>

<section class="module module__cards">

	<div class="container">

		<div class="module__cards__intro row row--center">

			<?php if($intro_heading): ?>
				<h2><?php echo $intro_heading; ?></h2>
			<?php endif; ?>
			<?php if($intro_content): ?>
				<?php echo $intro_content; ?>
			<?php endif; ?>

		</div>

		<div class="module__cards__cards row row--justified">

			<?php if( have_rows('cards') ) : ?>

				<?php while( have_rows('cards') ) : the_row(); ?>

					<?php
						// Content Vars

						$heading = get_sub_field('heading');
						$link = get_sub_field('link');
						$image = get_sub_field('image');
						$content = get_sub_field('content');
					?>

					<div class="card column column-m-6 <?php if($columns >= 3) : echo "column-t-4"; endif ?> column-d-<?php echo $columns; ?>">

						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>">
						<?php endif; ?>

							<div class="card__content" <?php if( $image ): ?>style="background: url('<?php echo $image; ?>') no-repeat center center / cover;"<?php endif; ?>>
								<?php if($heading): ?>
									<h3><?php echo $heading; ?></h3>
								<?php endif; ?>
								<?php if($content): ?>
									<?php echo $content; ?>
								<?php endif; ?>
							</div>

						<?php if( $link ): ?>
							</a>
						<?php endif; ?>

					</div>

				<?php endwhile; ?>

			<?php endif; ?>

		</div>

	</div>

</section>
