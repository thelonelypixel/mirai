<!-- Contact Layout -->

<?php

// vars

$tel = get_field('contact_number', 'options');
$email = get_field('email_address', 'options');
$hours = get_field('opening_hours', 'options');
$contact_heading = get_sub_field('contact_heading');
$contact_intro = get_sub_field('contact_intro');
$form_heading = get_sub_field('form_heading');
$form_intro = get_sub_field('form_intro');

?>

<section class="module module__contact">

	<div class="container">

		<div class="row row--justified">

			<div class="column column-m-12-nest column-t-4">

				<h4><?php echo $contact_heading ?></h4>
				<p><?php echo $contact_intro ?></p>

				<ul class="contact">
					<li>
						<a href="tel:<?php echo $tel ?>">
							<svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							        <g id="Contact" transform="translate(-304.000000, -958.000000)">
							            <g id="Get-in-touch" transform="translate(302.000000, 833.000000)">
							                <g id="Phone" transform="translate(2.000000, 125.000000)">
							                    <g id="phone">
							                        <circle id="Oval" fill="#3B5F8E" cx="17" cy="17" r="17"></circle>
							                        <path d="M17.2374603,12.3737895 C17.5353533,12.6716826 17.5360221,13.1539938 17.2367179,13.4532979 L16.4298176,14.2601982 C15.9839843,14.7060315 15.9848027,15.4311162 16.4325262,15.8788397 L19.1211603,18.5674738 C19.5717817,19.0180952 20.2935742,19.0164099 20.7398018,18.5701824 L21.5467021,17.7632821 C21.8450051,17.4649791 22.3274762,17.4638055 22.6262105,17.7625397 L25.5879852,20.7243145 C26.0346877,21.171017 26.036758,21.8931946 25.5861366,22.343816 L23.9709793,23.9589733 C22.930391,24.9995616 21.0413427,25.2732303 19.7780758,24.517276 C19.7780758,24.517276 17.7546146,23.6659044 14.563427,20.4747168 C11.3722394,17.2835291 10.4895293,15.2224393 10.4895293,15.2224393 C9.7535246,13.9470665 10.0032466,12.0668008 11.0410268,11.0290207 L12.656184,9.41386342 C13.1039075,8.96613995 13.8261751,8.96250435 14.2756855,9.4120148 L17.2374603,12.3737895 L17.2374603,12.3737895 Z M13.4668591,10.7581805 L12.1185228,12.1065167 C11.5673038,12.6577357 11.4740274,13.7608359 11.8093359,14.4607923 C11.8093359,14.4607923 11.9386768,14.7246537 12.0150096,14.8645737 C12.1573874,15.1255562 12.3475619,15.4375186 12.5911817,15.7969742 C13.3112392,16.8594035 14.3126227,18.0689203 15.640923,19.3972207 C16.9690138,20.7253115 18.1710488,21.720468 19.2209598,22.4303483 C19.5754143,22.670007 19.8819745,22.8560245 20.1374481,22.9943162 C20.2733058,23.067858 20.5605421,23.2097051 20.5605421,23.2097051 C21.1983728,23.543515 22.3323976,23.4425629 22.8934833,22.8814772 L24.2418195,21.5331409 L22.0868275,19.3781488 L21.8172979,19.6476784 C20.7747747,20.6902016 19.0881586,20.6894642 18.0436643,19.6449698 L15.3550302,16.9563357 C14.3130834,15.914389 14.3105403,14.2244834 15.3523216,13.1827021 L15.6218512,12.9131725 L13.4668591,10.7581805 L13.4668591,10.7581805 Z" id="Combined-Shape" fill="#FFFFFF" fill-rule="nonzero"></path>
							                    </g>
							                </g>
							            </g>
							        </g>
							    </g>
							</svg>
							<?php echo $tel ?>
						</a>
					</li>
					<li>
						<a href="mailto:<?php echo $email ?>">
							<svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							        <g id="Contact" transform="translate(-304.000000, -1002.000000)">
							            <g id="Get-in-touch" transform="translate(302.000000, 833.000000)">
							                <g id="Email" transform="translate(2.000000, 169.000000)">
							                    <g id="email">
							                        <circle id="Oval" fill="#3F6492" cx="17" cy="17" r="17"></circle>
							                        <path d="M10.4552036,11 L23.5447964,11 C24.3484831,11 25,11.6491802 25,12.450974 L25,22.6399351 C25,23.4412859 24.3479668,24.0909091 23.5447964,24.0909091 L10.4552036,24.0909091 C9.65151686,24.0909091 9,23.4417289 9,22.6399351 L9,12.450974 C9,11.6496232 9.65203317,11 10.4552036,11 Z M22.5170177,22.6364457 L18.4545455,18.5739735 L17.5142595,19.5142595 C17.2302418,19.7982771 16.7697582,19.7982771 16.4857405,19.5142595 L15.5454545,18.5739735 L11.4807277,22.6387004 C13.9252911,22.637322 20.0708437,22.6366608 22.5170177,22.6364457 Z M23.5452633,21.6076535 C23.5455989,19.6561007 23.5455369,15.4371757 23.5455265,13.4830327 L19.4830644,17.5454545 L23.5452633,21.6076535 Z M10.4544735,21.6078764 L14.5169356,17.5454545 L10.4547367,13.4832556 C10.4544011,15.4348084 10.454463,19.6537334 10.4544735,21.6078764 Z M22.5225513,12.4522068 C20.0791152,12.4535871 13.9247423,12.4542487 11.479697,12.4544637 L17,17.9955061 L22.5225513,12.4522068 Z" id="Combined-Shape" fill="#FFFFFF" fill-rule="nonzero"></path>
							                    </g>
							                </g>
							            </g>
							        </g>
							    </g>
							</svg>
							<?php echo $email ?>
						</a>
					</li>

				<h4>Opening hours</h4>
				<?php echo $hours ?>

			</div>

			<div class="contact__form column column-m-12 column-t-7">
				<h3><?php echo $form_heading ?></h3>
				<p><?php echo $form_intro ?></p>
				<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]'); ?>
			</div>

		</div>

	</div>

</section>
