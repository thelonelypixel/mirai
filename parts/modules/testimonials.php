<!-- Single Text Layout -->

<section class="module module__testimonials">

	<div class="container">

		<div class="row">

			<h4><?php the_sub_field('heading'); ?></h4>

			<?php if ( have_rows('testimonials') ): ?>
				<div class="testimonials-slider">
					<?php while ( have_rows('testimonials') ) : the_row(); ?>
						<div class="testimonial">
							<div class="testimonial__meta">
								<span class="author"><?php the_sub_field('author'); ?></span>
								<span class="company"><?php the_sub_field('company'); ?></span>
							</div>
							<div class="testimonial__content">
								<p><?php the_sub_field('testimonial'); ?></p>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>

		</div>

	</div>

</section>
