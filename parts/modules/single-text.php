<!-- Single Text Layout -->

<section class="module module__single-text">

	<?php if($container): ?>
		<div class="container">
	<?php endif; ?>

		<div class="row row--middle row--center">

			<?php if( $content ): ?>
				<div class="module__content">
					<?php echo $content; ?>

					<?php if( $button['type'] == 'Page' ): $button_url = $button['page_link'];
						elseif( $button['type'] =='Internal' ): $button_url = $button['internal_link'];
						else: $button_url = $button['external_link'];
					endif; ?>

					<?php if( $button['text']): ?>
						<a class="button" href="<?php echo $button_url; ?>" <?php if($button['type'] == "External") : ?>target="_blank" rel="noopener noreferrer"<?php endif; ?>><?php echo $button['text']; ?></a>
					<?php endif; ?>
				</div>
			<?php endif; ?>

		</div>

	<?php if($container): ?>
		</div>
	<?php endif; ?>

</section>
