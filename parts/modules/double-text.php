<section class="module module__double-text">

	<?php if($container): ?>
		<div class="container">
	<?php endif; ?>

		<div class="row row--middle row-collapse">

			<?php if( $content_l ): ?>
				<div class="module__content column column-m-12-nest column-t-6-nest">
					<?php echo $content_l; ?>
				</div>
			<?php endif; ?>

			<?php if( $content_r ): ?>
				<div class="module__content column column-m-12-nest column-t-6-nest">
					<?php echo $content_r; ?>
				</div>
			<?php endif; ?>

		</div>

	<?php if($container): ?>
		</div>
	<?php endif; ?>

</section>
