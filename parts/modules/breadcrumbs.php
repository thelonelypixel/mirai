<?php if ( function_exists('yoast_breadcrumb') ): ?>
	<section class="module module__breadcrumbs">
		<div class="container">
			<div class="breadcrumbs">
				<?php yoast_breadcrumb('<p id="breadcrumbs">','</p>'); ?>
			</div>
		</div>
	</section>
<?php endif; ?>
