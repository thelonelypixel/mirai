<section class="module module__news">

	<div class="container">

		<h3><?php echo $heading ?></h3>

		<?php
		$args = array(
			'posts_per_page' => '3'
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) : ?>

			<div class="module__news__posts row row--justified">
				<?php while ( $query->have_posts() ) : $query->the_post(); ?>
					<a href="<?php the_permalink(); ?>" class="module__news__posts__post">
						<?php the_post_thumbnail('blog-thumb'); ?>
						<div class="post-content">
							<h4><?php the_title(); ?></h4>
							<span><?php the_time( get_option( 'date_format' ) ); ?></span>
						</div>
					</a>
				<?php endwhile; ?>
			</div>

		<?php endif; ?>

		<?php wp_reset_postdata(); ?>

	</div>

</section>
