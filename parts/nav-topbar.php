<div class="row row--justified">
	<a class="brand" href="<?= esc_url(home_url('/')); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="<?php bloginfo('name'); ?>" />	
	</a>

	<!-- Desktop Navigation -->
	<nav class="nav-primary">
		<?php mirai_top_nav(); ?>
	</nav>

	<!-- Mobile Navigation -->
	<div class="mobile-trigger">
		<span></span>
	</div>
</div>
