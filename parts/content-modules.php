<?php

// check if the flexible content field has rows of data
if( have_rows('modules') ): ?>

		<?php // loop through the rows of data
		while ( have_rows('modules') ) : the_row();

			// Hero Fields
			$hero_type = get_sub_field('hero_type');
			$hero_image = get_sub_field('hero_image');
			$hero_title = get_sub_field('hero_heading');
			$hero_content = get_sub_field('hero_content');
			$button = get_sub_field('button');

		 	// Option Fields
			$container = get_sub_field('container');
			$flip = get_sub_field('flip_layout');

			// Content Fields
			$heading = get_sub_field('heading');
			$image = get_sub_field('image');
			$content = get_sub_field('content');
			$content_l = get_sub_field('content_left');
			$content_r = get_sub_field('content_right');
			$button = get_sub_field('button');

			if( get_row_layout() == 'hero' ):

				include(locate_template('parts/modules/hero.php'));

			elseif( get_row_layout() == 'image_text' ):

				include(locate_template('parts/modules/image-text.php'));

			elseif( get_row_layout() == 'double_text' ):

				include(locate_template('parts/modules/double-text.php'));

			elseif( get_row_layout() == 'single_text' ):

				include(locate_template('parts/modules/single-text.php'));

			elseif( get_row_layout() == 'cards' ):

				include(locate_template('parts/modules/cards.php'));

			elseif( get_row_layout() == 'breadcrumbs' ):

				include(locate_template('parts/modules/breadcrumbs.php'));

			elseif( get_row_layout() == 'latest_news' ):

				include(locate_template('parts/modules/latest-news.php'));

			elseif( get_row_layout() == 'contact' ):

				include(locate_template('parts/modules/contact.php'));

			elseif( get_row_layout() == 'testimonials' ):

				include(locate_template('parts/modules/testimonials.php'));

			endif;

		endwhile; ?>

<?php else :

    // no layouts found

endif;

?>
