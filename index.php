<?php get_header(); ?>

	<main id="main" role="main">

		<?php if ( have_posts() ) : ?>

			<section class="module module__news">

				<div class="container">

					<div class="module__news__posts row row--justified">

						<?php while ( have_posts() ) : the_post(); ?>

						<a href="<?php the_permalink(); ?>" class="module__news__posts__post">
							<?php the_post_thumbnail('blog-thumb'); ?>
							<div class="post-content">
								<h4><?php the_title(); ?></h4>
								<span><?php the_time( get_option( 'date_format' ) ); ?></span>
							</div>
						</a>

						<?php endwhile; ?>

						<div class="nav-previous alignleft"><?php previous_posts_link( 'Older posts' ); ?></div>
						<div class="nav-next alignright"><?php next_posts_link( 'Newer posts' ); ?></div>

					</div>

				</div>

			</section>

		<?php else :

			get_template_part( 'parts/content', 'none' );

		endif; ?>

	</main>

<?php get_footer(); ?>
