<?php get_header(); ?>

	<main id="main" role="main">

		<?php if ( have_posts() ) : ?>

			<header>
				<?php the_archive_title( '<h1">', '</h1>' ); ?>
			</header>

			<?php 
			while ( have_posts() ) : the_post(); 
				get_template_part( 'parts/content', 'content' );
			endwhile; 

		else :

			get_template_part( 'parts/content', 'none' );

		endif; ?>

	</main>

<?php get_footer(); ?>