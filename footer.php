
	<footer class="site-footer" role="contentinfo">

		<div class="site-footer__top row">

			<div class="container">

			</div>
			
		</div>

		<div class="site-footer__bottom">

			<div class="container">

				<div class="legal">
					<p>© <?php echo get_bloginfo('name'); ?> <?php echo date("Y"); ?>. All Rights Reserved.</p>
				</div>

				<div class="site-by">
					<p><a href="https://www.thelonelypixel.co.uk">Web Design Hertfordshire</a> by The Lonely Pixel</a></p>
				</div>
			
			</div>

		</div>
	</footer>

	<div class="offcanvas">
		<div class="row row--middle row--justified">
			<div class="column column-m-12-nest">
				<nav class="nav-mobile">
					<?php mirai_top_nav(); ?>
				</nav>
			</div>
		</div>
	</div>

	<?php wp_footer(); ?>

</body>
</html>
